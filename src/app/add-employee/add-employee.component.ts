import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  form:  FormGroup;
  public id: number;
  public employee: any;
  
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.form=new FormGroup({
      name : new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.pattern('[a-zA-Z ]*')
      ])),
      location : new FormControl("", Validators.required),
      email : new FormControl("", Validators.compose([
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])),
      mobile : new FormControl("",  Validators.compose([
        Validators.required,
        Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')
      ]))
    })
  }

  addEmployee(emp){
    this.employeeService.getEmployees().subscribe((res: any)=>{
      if(res.length == 0)
        this.id = 1
      else
        this.id = res[res.length-1].id+1
    })
    this.employee = {id: this.id, name: emp.name, location: emp.location, email: emp.email, mobile: emp.mobile }
    this.employeeService.addEmployee(this.employee).subscribe((res) =>{
      this.router.navigate(['/employees']);
    });
  }

}
