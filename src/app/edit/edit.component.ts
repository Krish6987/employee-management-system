import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormGroup, Validators, FormControl } from '@angular/forms';

import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form:  FormGroup;
  public id: String;
  public employee: any;

  constructor(private route: ActivatedRoute, private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {

    this.form=new FormGroup({
      name : new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.pattern('[a-zA-Z ]*')
      ])),
      location : new FormControl("", Validators.required),
      email : new FormControl("", Validators.compose([
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])),
      mobile : new FormControl("",  Validators.compose([
        Validators.required,
        Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')
      ]))
    })

    this.id = this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(this.id).subscribe((res: any)=>{
      this.employee = res;
      this.id = this.employee.id;
      this.form.setValue({
        name: this.employee.name,
        location: this.employee.location,
        email: this.employee.email,
        mobile: this.employee.mobile
      })
    }) 
  }
  editEmployee(emp){
    this.employee = {id: this.id, name: emp.name, location: emp.location, email: emp.email, mobile: emp.mobile };
    this.employeeService.editEmployee(this.employee).subscribe((res) =>{
      this.router.navigate(['/employees'])
    });
  }

}
