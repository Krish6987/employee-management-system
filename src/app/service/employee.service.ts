import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public url = 'api/employees';

  constructor(private http: HttpClient) { }

  getEmployees() {
    return this.http.get<JSON>(this.url);
  }

  getEmployee(id: String){
    return this.http.get<any>(this.url+"/"+id);
  }

  addEmployee(employee: {id: number, name: String, location: String, email: String, mobile: String}){
    return this.http.post(this.url, employee);
  }

  deleteEmployee(id:String){
    return this.http.delete(this.url+"/"+id);
  }

  editEmployee(employee: {id: number, name: String, location: String, email: String, mobile: String}) {
    return this.http.put(this.url+'/' + employee.id, employee);
  }

}
